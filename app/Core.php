<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Bootstrap.php
 */

/**
 * global functions
 *
 * @param bool $return
 * @param bool $html
 * @param bool $showFirst
 * @return string
 */
function DebugBacktrace($return=false, $html=true, $showFirst=false)
{
    $d = debug_backtrace();
    $out = '';
    if ($html) $out .= "<pre>";
    foreach ($d as $i=>$r) {
        if (!$showFirst && $i==0) {
            continue;
        }
        @$out .= "[$i] {$r['file']}:{$r['line']}\n";
    }
    if ($html) $out .= "</pre>";
    if ($return) {
        return $out;
    } else {
        echo $out;
    }
}

/**
 * Class Core
 */
class Core
{
    /**
     * @var null
     */
    static protected $_init = NULL;

    /**
     * @var null
     */
    static protected $_db = NULL;

    private function __construct(){}

    /**
     * @return Core|null
     */
    static public function init()
    {
        if (!self::$_init)
        {
            self::$_init = new Core();

            spl_autoload_register(array(self::init(), 'autoload'));
        }

        return self::$_init;
    }

    /**
     * @return null|Zend_Db_Adapter_Abstract
     */
    static public function DB()
    {
        if (!self::$_db)
        {
            $db = Zend_Db::factory('Pdo_Mysql', include_once JCAT_ROOT . DIRECTORY_SEPARATOR . 'config.php');

            self::$_db = $db;
        }

        return self::$_db;
    }

    /**
     * @param string $class
     */
    public function autoload($class)
    {
        $classArr = explode('_', $class);

        $classFile = str_replace(' ', DIRECTORY_SEPARATOR, ucwords(str_replace('_', ' ', $class)));

        switch(strtolower($classArr[0]))
        {
            case 'zend':
                $classFile = 'lib' . DIRECTORY_SEPARATOR . $classFile;
                break;
            default:
                break;
        }

        $classFile.= '.php';

        try
        {
            if (!file_exists($classFile))
            {
                throw new Core_Exception("File $classFile does not exits");
            }
            else
            {
                return include $classFile;
            }
        }
        catch(Exception $e)
        {
            $e->getErrorMessage();
        }

    }

    public function run()
    {
        try
        {
            if ($_SERVER['REQUEST_URI']!='/')
            {
                header("HTTP/1.0 404 Not Found");
                throw new Core_Exception("Page does not exist");
            }
            else
            {
                $request = new App_Request();
                $request->match();
            }
        }
        catch(Exception $e)
        {
            if (method_exists($e, 'getErrorMessage'))
            {
                $e->getErrorMessage();
            }
            else
            {
                echo '<pre><br/>';
                var_dump($e->getMessage());
            }
        }
    }
}

/**
 * Class Core_Exception
 */
class Core_Exception extends Exception
{
    /**
     * @return string
     */
    public function getErrorMessage()
    {
        $message = parent::getMessage();

        if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE']) && (bool) $_SERVER['MAGE_IS_DEVELOPER_MODE'])
        {
            print $message;
            echo '<pre>';
            DebugBacktrace();
            die();
        }
        else
        {
            return $message;
        }
    }
}


