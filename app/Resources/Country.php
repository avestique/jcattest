<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Country.php
 */

class App_Resources_Country extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'shower' => array(
                'description' => 'расположение (возможные значения — «в доме», «на улице»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getShower(),
                'type'        => 'varchar'
            ),
            'toilet' => array(
                'description' => 'расположение (возможные значения — «в доме», «на улице»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getToilet(),
                'type'        => 'varchar'
            ),
            'pmg' => array(
                'description' => 'возможность ПМЖ (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'electricity-supply' => array(
                'description' => 'электроснабжение (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'sewerage-supply' => array(
                'description' => 'канализация (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'water-supply' => array(
                'description' => 'наличие водопровода (строго ограниченные значения — «да»/«нет», « true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'heating-supply' => array(
                'description' => 'наличие отопления (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/-)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'sauna' => array(
                'description' => 'наличие сауны/бани (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'billiard' => array(
                'description' => 'наличие бильярда (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'pool' => array(
                'description' => 'наличие бассейна (строго ограниченные значения — «да»/«нет», «true»/ «false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'kitchen' => array(
                'description' => 'Наличие кухни (строго ограниченные значения — «да»/«нет», «true»/ «false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'gas-supply' => array(
                'description' => 'подключение к газовым сетям (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            )
        );

        $this->setConfig($config);
    }

    /**
     * @return array
     */
    public function getDependency()
    {
        return array(
            'дом',
            'flat',
            'house',
            'cottage',
            'коттедж',
            'townhouse',
            'часть дома',
            'дом с участком',
            'участок',
            'земельный участок',
            'house with lot',
            'lot',
        );
    }

    /**
     * @return array
     */
    static public function getToilet()
    {
        return array(
            "в доме",
            "на улице"
        );
    }

    /**
     * @return array
     */
    static public function getShower()
    {
        return array(
            "в доме",
            "на улице"
        );
    }
}