<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Object.php
 */

class App_Resources_Object extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'image' => array(
                'description' => 'фотография (может быть несколько тегов)',
                'requried'    => false,
                'type'        => 'image'
            ),
            'renovation' => array(
                'description' => 'ремонт (рекомендуемые значения — «евро», «дизайнерский»)',
                'requried'    => false,
                'fixed'       => false,
                'values'      => $this->getRenovation(),
                'type'        => 'varchar'
            ),
            'description' => array(
                'description' => 'дополнительная информация (описание в свободной форме, оставленное подателем объявления)',
                'requried'    => false,
                'type'        => 'text'
            ),
            'area' => array(
                'description' => 'общая площадь',
                'requried'    => false,
                'type'        => 'size'
            ),
            'living-space' => array(
                'description' => 'жилая площадь (при продаже комнаты — площадь комнаты)',
                'requried'    => false,
                'type'        => 'size'
            ),
            'kitchen-space' => array(
                'description' => 'площадь кухни',
                'requried'    => false,
                'type'        => 'size'
            ),
            'lot-area' => array(
                'description' => 'площадь участка в случае предложения «дом с участком» или «участок»',
                'requried'    => false,
                'type'        => 'size',
                //'depends'     => array('general' => array('category' => array('дом с участком', 'участок'))),
            ),
            'lot-type' => array(
                'description' => 'тип участка (рекомендуемые значения — «ИЖC», «садоводство»)',
                'requried'    => false,
                'fixed'       => false,
                'values'      => $this->getType(),
                'type'        => 'varchar'
            )
            /*
             * 'fields'      => array(
                    'value' => array(
                        'description' => 'площадь (числовое значение)',
                        'requried'    => false
                    ),
                    'unit' => array(
                        'description' => 'единица площади',
                        'requried'    => false,
                        'fixed'       => false,
                        'values'      => $this->getUnits()
                    )
                )
             */
        );

        $this->setConfig($config);


    }

    /**
     * @return array
     */
    static public function getType()
    {
        return array(
            "ИЖC",
            "садоводство"
        );
    }

    /**
     * @return array
     */
    static public function getRenovation()
    {
        return array(
            "евро",
            "дизайнерский"
        );
    }

    /**
     * @return array
     */
    static public function getUnits()
    {
        return array(
            "кв. м",
            "sq.m"
        );
    }
}