<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Saler.php
 */

class App_Resources_Seller extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'name' => array(
                'description' => 'имя агента/продавца',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'phone' => array(
                'description' => 'телефон агента/продавца:(каждому тегу соответствует один номер телефона, если номеров несколько, каждый из них необходимо передавать в отдельном теге phone)',
                'requried'    => true,
                'type'        => 'varchar'
            ),
            'category' => array(
                'description' => 'тип продавца (строго ограниченные значения — «владелец», «агентство», «owner», «agency»)',
                'requried'    => false,
                'fixed'       => true,
                'values'      => $this->getCategories(),
                'type'        => 'varchar'
            ),
            'organization' => array(
                'description' => 'название агентства',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'agency-id' => array(
                'description' => 'внутренний ID агентства в базе партнера',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'url' => array(
                'description' => 'сайт агентства',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'email' => array(
                'description' => 'электронный адрес продавца',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'partner' => array(
                'description' => 'название партнера, предоставившего объявление',
                'requried'    => false,
                'type'        => 'varchar'
            )
        );

        $this->setConfig($config);
    }

    /**
     * @return array
     */
    static public function getCategories()
    {
        return array(
            "владелец",
            "агентство",
            "owner",
            "agency"
        );
    }

    /**
     * @param $offer
     * @return array
     */
    public function analize($offer)
    {
        $data = array();

        $node = 'sales-agent';

        if (isset($offer->$node))
        {
            foreach($this->getConfig() as $idItem => $item)
            {
                if (isset($item['requried']) && $item['requried'] && !isset($offer->$node->{$idItem}))
                {
                    App_Request::setValidated(false);
                    break;
                }

                if (isset($offer->$node->{$idItem}))
                {
                    $field = $offer->$node->{$idItem}->__toString();

                    if (isset($item['fixed']) && $item['fixed'] && !in_array($field, isset($item['values']) ? $item['values'] : $item['yesno']))
                    {
                        App_Request::setValidated(false);
                        break;
                    }

                    if (strlen($field) > 0 ? $field : NULL)
                        $data["varchar"]['agency_' . $idItem] = $field;
                }
            }
        }

        if (!$data)
            App_Request::setValidated(false);


        return $data;
    }
}