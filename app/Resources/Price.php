<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Currency.php
 */

class App_Resources_Price extends App_Resources_Abstract
{
    public function __construct()
    {
         $config = array(
             'value' => array(
                 'description' => 'цена (сумма указывается без пробелов)',
                 'requried'    => true
             ),
             'currency' => array(
                 'description' => 'валюта, в которой измеряется стоимость ( «RUR», «RUB», «EUR», «USD», «UAH», «BYR», «KZT»)',
                 'requried'    => true,
                 'fixed'       => true,
                 'values'      => $this->getCurrency()
             ),
             'period' => array(
                 'description' => 'в случае сдачи недвижимости в аренду — промежуток времени (рекомендуемые значения — «день», «месяц», «day», «month»)',
                 'requried'    => false,
                 'fixed'       => false,
                 'values'      => $this->getPeriod()
             ),
             'unit' => array(
                 'description' => 'единица площади (рекомендуемые значения — «кв. м», «гектар», «cотка», «sq.m», «hectare»)',
                 'requried'    => false,
                 'fixed'       => false,
                 'values'      => $this->getUnits()
             )
         );

         $this->setConfig($config);
    }

    /**
     * @return array
     */
    static public function getCurrency()
    {
        return array(
            "RUR",
            "RUB",
            "EUR",
            "USD",
            "UAH",
            "BYR",
            "KZT",
        );
    }

    /**
     * @return array
     */
    static public function getPeriod()
    {
        return array(
            "день",
            "месяц",
            "day",
            "month"
        );
    }

    /**
     * @return array
     */
    static public function getUnits()
    {
        return array(
            "кв. м",
            "гектар",
            "cотка",
            "sq.m",
            "hectare"
        );
    }

    /**
     * @param $offer
     * @return array
     */
    public function analize($offer)
    {
        $data = array();


        if (isset($offer->price))
        {
            foreach($offer->price as $price)
            {
                $dataPrice = array();

                foreach($this->getConfig() as $idItem => $item)
                {
                    if (isset($item['requried']) && $item['requried'] && !isset($price->{$idItem}))
                    {
                        App_Request::setValidated(false);
                        break;
                    }

                    if (isset($price->{$idItem}))
                    {
                        $field = $price->{$idItem}->__toString();

                        if (isset($item['fixed']) && $item['fixed'] && !in_array($field, isset($item['values']) ? $item['values'] : $item['yesno']))
                        {
                            App_Request::setValidated(false);
                            break;
                        }

                        $dataPrice[$idItem] = $field;
                    }
                }

                $data[] = $dataPrice;
            }

        }

        if (!$data)
        {
            App_Request::setValidated(false);
        }

        return array('price' => $data);
    }
}