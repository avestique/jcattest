<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you  d not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Interface.php
 */

abstract class App_Resources_Abstract implements App_Resources_Interface
{
    /**
     * @var array
     */
    protected $_config = array();

    /**
     * @param array $arr
     * @return $this
     */
    public function setConfig(array $arr)
    {
        $this->_config = $arr;

        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * @return array
     */
    static public function getFieldTypes()
    {
        return array(
            'location',
            'sales-agent',
            'price',
            'image',
            'area',
            'lot-area'
        );
    }

    /**
     * @return array
     */
    public function getDependency()
    {
        return array();
    }

    /**
     * @param $offer
     * @return array
     */
    public function analize($offer)
    {
        $data = array();

        foreach($this->getConfig() as $idItem => $item)
        {
            if (isset($item['requried']) && $item['requried'] && !isset($offer->{$idItem}))
            {
                App_Request::setValidated(false);
                break;
            }

            if (isset($offer->{$idItem}))
            {
                $field = $offer->{$idItem}->__toString();

                if (isset($item['fixed']) && $item['fixed'] && !in_array($field, isset($item['values']) ? $item['values'] : $item['yesno']))
                {
                    App_Request::setValidated(false);
                    break;
                }
                else if (isset($item['validate']))
                {
                    $pattern = $item['validate'];
                    $validate_arr = explode(';', $pattern);
                    foreach($validate_arr as $validate_item)
                    {
                        list($name, $val) = explode(':', $validate_item);

                        switch($name)
                        {
                            case 'length':
                                $length = strlen($field);

                                if ($field && $length!=$val)
                                {
                                    App_Request::setValidated(false);
                                }

                                break;
                            default:
                                break;
                        }
                    }
                }

                switch($item['type'])
                {
                    case 'varchar':
                        $type = 'varchar';
                        $field = strlen($field) > 0 ? $field : NULL;
                        break;
                    case 'int':
                        $type = 'int';
                        $field = (int) $field;
                        break;
                    case 'bool':
                        $type = 'int';
                        $field = in_array($field, App_Resources_General::getYes()) ? 1 : 0;
                        break;
                    case 'text':
                        $field = strlen($field) > 0 ? $field : NULL;
                        $type = 'text';
                        break;
                    case 'image':
                        //$field = strlen($field) > 0 ? $field : NULL;
                        $arr = array();
                        foreach($offer->{$idItem} as $img)
                        {
                            $arr[] = $img->__toString();
                        }
                        $field = $arr;
                        $type = 'image';
                        break;
                    case 'price':
                        $type = 'price';
                        break;
                    case 'entity':
                        $type = 'entity';
                        break;
                    case 'size':
                        $field = array(
                            $offer->{$idItem}->value->__toString(),
                            $offer->{$idItem}->unit->__toString()
                        );
                        $type = 'int';
                        break;
                    default:
                        break;
                }

                //echo '<pre><br/>'; var_dump($item, $field);

                if ($field!==NULL)
                {
                    if (!in_array($idItem, array('image', 'price')))
                        $data[$type][$idItem] = $field;
                    else if (in_array($idItem, array('image')))
                        $data[$type] = $field;
                    else
                        $data[$type][] = $field;
                }

            }
        }

        return $data;
    }
}