<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Location.php
 */

class App_Resources_Location extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'country' => array(
                'description' => 'страна',
                'requried'    => true,
                'type'        => 'varchar'
            ),
            'region' => array(
                'description' => 'для России — название субъекта РФ, для Белорусии и Казахстана — название области, для Украины — название области или автономной республики, если объект находится в Крыму.',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'district' => array(
                'description' => 'для России — название района субъекта РФ, для Белорусии, Казахстана, Украины — название района области',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'locality-name' => array(
                'description' => 'название города, деревни, поселка и т.д.',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'sub-locality-name' => array(
                'description' => 'район города',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'sub-locality-name' => array(
                'description' => 'район города',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'non-admin-sub-locality' => array(
                'description' => 'неадминистративный район города или ориентир (список городов, для которых поддерживается этот параметр, уточняйте по адресу info@realty.yandex.ru)',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'address' => array(
                'description' => 'улица, дом',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'distance' => array(
                'description' => 'расстояние по шоссе до МКАД (указывается в км)',
                'requried'    => false,
                'type'        => 'int'
            ),
            'direction' => array(
                'description' => 'шоссе (только для Москвы)',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'longitude' => array(
                'description' => 'географические координаты (долгота)',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'latitude' => array(
                'description' => 'географические координаты (широта)',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            /*'metro' => array(
                'description' => 'ближайшая станция метро (если таковых несколько, каждый указывается в отдельном теге)',
                'requried'    => false,
                'type'        => 'varchar'
            ),*/
            'name' => array(
                'description' => 'название станции метро',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'time-on-transport' => array(
                'description' => 'время до метро в минутах на транспорте',
                'requried'    => false,
                'type'        => 'int'
            ),
            'time-on-foot' => array(
                'description' => 'время до метро в минутах пешком',
                'requried'    => false,
                'type'        => 'int'
            ),
            'railway-station' => array(
                'description' => 'ближайшая ж/д станция (для загородной недвижимости)',
                'requried'    => false,
                'type'        => 'varchar'
            )
        );

        $this->setConfig($config);
    }

    /**
     * Types
     *
     * @return array
     */
    static public function getTypes()
    {
        return array(
            'продажа',
            'аренда'
        );
    }

    /**
     * Types
     *
     * @return array
     */
    static public function getPropertyTypes()
    {
        return array(
            'жилая'
        );
    }

    /**
     * Categories
     *
     * @return array
     */
    static public function getCategories()
    {
        return array(
            "комната",
            "квартира",
            "дом",
            "участок",
            "flat",
            "room",
            "house",
            "cottage",
            "коттедж",
            "townhouse",
            "таунхаус",
            "часть дома",
            "house with lot",
            "дом с участком",
            "дача",
            "lot",
            "участок",
            "земельный участок"
        );
    }

    /**
     * @param array $except
     * @return array
     */
    static public function getNo(array $except = array())
    {
        return array_diff(array(
            "нет",
            "false",
            "0",
            "-"
        ), $except);
    }

    /**
     * @param array $except
     * @return array
     */
    static public function getYes(array $except = array())
    {
        return array_diff(array(
            "да",
            "true",
            "1",
            "+"
        ), $except);
    }

    /**
     * @param $offer
     * @return array
     */
    public function analize($offer)
    {
        $data = array();

        if (isset($offer->location))
        {
            foreach($this->getConfig() as $idItem => $item)
            {

                if (isset($item['requried']) && $item['requried'] && !isset($offer->location->{$idItem}))
                {
                    App_Request::setValidated(false);
                    break;
                }

                if (isset($offer->location->{$idItem}))
                {
                    $field = $offer->location->{$idItem}->__toString();

                    if (isset($item['fixed']) && $item['fixed'] && !in_array($field, isset($item['values']) ? $item['values'] : $item['yesno']))
                    {
                        App_Request::setValidated(false);
                        break;
                    }

                    switch($item['type'])
                    {
                        case 'varchar':
                            $type = 'varchar';
                            $field = strlen($field) > 0 ? $field : NULL;
                            break;
                        case 'int':
                        case 'bool':
                            $type = 'int';
                            $field = (int) $field;
                            break;
                    }

                    if ($field)
                        $data[$type]['location_' . $idItem] = $field;
                }
            }

            if (isset($offer->location->metro))
            {
                foreach($this->getConfig() as $idItem => $item)
                {
                    if (isset($offer->location->metro->{$idItem}))
                    {
                        $field = $offer->location->metro->{$idItem}->__toString();

                        if (isset($item['fixed']) && $item['fixed'] &&  !in_array($field, isset($item['values']) ? $item['values'] : $item['yesno']))
                        {
                            App_Request::setValidated(false);
                            break;
                        }

                        switch($item['type'])
                        {
                            case 'varchar':
                                $type = 'varchar';

                                $field = strlen($field) > 0 ? $field : NULL;
                                break;
                            case 'int':
                            case 'bool':
                                $type = 'int';
                                $field = (int) $field;
                                break;
                        }

                        if ($field!==NULL)
                            $data[$type]['metro_' . $idItem] = $field;
                    }
                }
            }
        }

        if (!$data)
        {
            App_Request::setValidated(false);
        }

        return $data;
    }
}