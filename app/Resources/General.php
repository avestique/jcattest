<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Resource.php
 */

class App_Resources_General extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'type' => array(
                'description' => 'тип сделки',
                'requried'    => true,
                'fixed'       => true,
                'values'      => $this->getTypes(),
                'type'        => 'entity'
            ),
            'property-type' => array(
                'description' => 'тип недвижимости',
                'requried'    => true,
                'fixed'       => false,
                'values'      => $this->getPropertyTypes(),
                'type'        => 'varchar'
            ),
            'category' => array(
                'description' => 'категория объекта',
                'requried'    => true,
                'fixed'       => true,
                'values'      => $this->getCategories(),
                'type'        => 'varchar'
            ),
            'url' => array(
                'description' => 'URL страницы с объявлением',
                'requried'    => true,
                'type'        => 'entity'
            ),
            'creation-date' => array(
                'description' => 'дата создания объявления формат даты такой же, как в поле generation-date',
                'requried'    => true,
                'pattern'     => '',
                'type'        => 'entity'
            ),
            'last-update-date' => array(
                'description' => 'дата последнего обновления объявления формат даты такой же, как в поле generation-date',
                'requried'    => false,
                'pattern'     => '',
                'type'        => 'entity'
            ),
            'expire-date' => array(
                'description' => 'дата и время, до которых объявление актуально формат даты такой же, как в поле generation-date',
                'requried'    => false,
                'pattern'     => '',
                'type'        => 'entity'
            ),
            'payed-adv' => array(
                'description' => 'оплаченное объявление',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive($this->getYes(), $this->getNo()),
                'type'        => 'bool'
            ),
            'manually-added'  => array(
                'description' => 'объявление добавлено вручную',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive($this->getYes(), $this->getNo()),
                'type'        => 'bool'
            ),

            'not-for-agents' => array(
                'description' => 'просьба агентам не звонить (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'haggle' => array(
                'description' => 'торг (строго ограниченные значения — «да»/«нет», « true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'mortgage' => array(
                'description' => 'ипотека (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(array("+")), App_Resources_General::getNo(array("-"))),
                'type'        => 'bool'
            ),
            'prepayment' => array(
                'description' => 'предоплата (указывается числовое значение в процентах без знака %)',
                'requried'    => false,
                'type'        => 'int'
            ),
            'rent-pledge' => array(
                'description' => 'Залог (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'agent-fee' => array(
                'description' => 'комиссия арендатора (указывается числовое значение в процентах без знака %)',
                'requried'    => false,
                'type'        => 'int'
            ),
            'with-pets' => array(
                'description' => 'для аренды: можно ли с животными (строго ограниченные значения — «да»/ «нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'with-children' => array(
                'description' => 'для аренды: можно ли с детьми (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'    => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            )
        );

        $this->setConfig($config);
    }

    /**
     * Types
     *
     * @return array
     */
    static public function getTypes()
    {
        return array(
            'продажа',
            'аренда'
        );
    }

    /**
     * Types
     *
     * @return array
     */
    static public function getPropertyTypes()
    {
        return array(
            'жилая'
        );
    }

    /**
     * Categories
     *
     * @return array
     */
    static public function getCategories()
    {
        return array(
            "комната",
            "квартира",
            "дом",
            "flat",
            "room",
            "house",
            "cottage",
            "коттедж",
            "townhouse",
            "таунхаус",
            "часть дома",
            "house with lot",
            "дом с участком",
            "дача",
            "lot",
            "участок",
            "земельный участок"
        );
    }

    /**
     * @param array $except
     * @return array
     */
    static public function getNo(array $except = array())
    {
        return array_diff(array(
            "нет",
            "false",
            "0",
            "-"
        ), $except);
    }

    /**
     * @param array $except
     * @return array
     */
    static public function getYes(array $except = array())
    {
        return array_diff(array(
            "да",
            "true",
            "1",
            "+"
        ), $except);
    }
}