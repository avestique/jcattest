<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Building.php
 */

class App_Resources_Building extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'floors-total' => array(
                'description' => 'общее количество этажей в доме',
                'requried'    => false,
                'type'        => 'int'
            ),
            'building-name' => array(
                'description' => 'название жилого комплекса (для новостроек)',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'building-series' => array(
                'description' => 'серия дома',
                'requried'    => false,
                'type'        => 'varchar'
            ),
            'built-year' => array(
                'description' => 'год постройки. Для новостроек — год сдачи (год необходимо указывать полностью, например, 1996, а не 96)',
                'requried'    => false,
                'type'        => 'varchar',
                'validate'    => 'length:4'
            ),
            'ceiling-height' => array(
                'description' => 'высота потолков',
                'requried'    => false,
                'type'        => 'int'
            ),
            'building-type' => array(
                'description' => 'тип дома (рекомендуемые значения — «кирпичный», «монолит», «панельный»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getType(),
                'type'        => 'varchar'
            ),
            'lift' => array(
                'description' => 'наличие лифта (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'rubbish-chute' => array(
                'description' => 'наличие мусоропровода (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'is-elite' => array(
                'description' => 'элитность (строго ограниченные значения — «да»/«нет», « true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'parking' => array(
                'description' => 'наличие парковки (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'alarm' => array(
                'description' => 'наличие охраны/сигнализации (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'building-state' => array(
                'description' => 'стадия строительства дома (для новостроек) (строго ограниченные значения: «unfinished» — строится; «built» — дом построен, но не сдан; «hand-over» — сдан в эксплуатацию)',
                'requried'    => false,
                'fixed'       => true,
                'values'       => $this->getState(),
                'type'        => 'varchar'
            ),
            'ready-quarter' => array(
                'description' => 'для новостроек — квартал сдачи дома (строго ограниченные значения — «1», «2», «3», «4»)',
                'requried'    => false,
                'fixed'       => true,
                'values'      => $this->getQuartal(),
                'type'        => 'int'
            )
        );

        $this->setConfig($config);
    }

    /**
     * @return array
     */
    public function getDependency()
    {
        return array();
    }

    /**
     * @return array
     */
    static public function getType()
    {
        return array(
            'кирпичный',
            'монолит',
            'панельный'
        );
    }

    /**
     * @return array
     */
    static public function getState()
    {
        return array(
            'unfinished',
            'built',
            'hand-over'
        );
    }

    /**
     * @return array
     */
    static public function getQuartal()
    {
        return array(1,2,3,4);
    }
}