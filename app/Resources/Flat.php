<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Flat.php
 */

class App_Resources_Flat extends App_Resources_Abstract
{
    public function __construct()
    {
        $config = array(
            'new-flat' => array(
                'description' => 'устанавливается, если квартира продается в новостройке (строго ограниченные значения — «да», «true», «1», «+»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(array('true','да')), App_Resources_General::getNo(array('false','нет'))),
                'type'        => 'bool'
            ),
            'rooms' => array(
                'description' => 'общее количество комнат в квартире',
                'requried'    => true,
                'type'        => 'int'
            ),
            'rooms-offered' => array(
                'description' => 'для продажи и аренды комнат: количество комнат, участвующих в сделке',
                'requried'    => true,
                'type'        => 'int'
            ),
            'open-plan' => array(
                'description' => 'свободная планировка (строго ограниченные значения — «да», «true», «1», «+»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(array('true','да')), App_Resources_General::getNo(array('false','нет'))),
                'type'        => 'bool'
            ),
            'rooms-type' => array(
                'description' => 'тип комнат (рекомендуемые значения — «смежные», «раздельные»)',
                'requried'    => false,
                'fixed'       => false,
                'values'      => $this->getRoomViews(),
                'type'        => 'varchar'
            ),
            'phone' => array(
                'description' => 'наличие телефона (строго ограниченные значения — «да»/ «нет», «true»/ «false», «1»/ «0», «+»/ «-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'internet' => array(
                'description' => 'наличие интернета (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'room-furniture' => array(
                'description' => 'наличие мебели (строго ограниченные значения — «да»/ «нет», «true»/«false», «1»/ «0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'kitchen-furniture' => array(
                'description' => 'наличие мебели на кухне (строго ограниченные значения — «да»/«нет», « true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'television' => array(
                'description' => 'наличие телевизора (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/ «-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'washing-machine' => array(
                'description' => 'наличие стиральной машины (строго ограниченные значения — «да»/«нет», « true»/ «false», «1»/ «0», «+»/ «-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'refrigerator' => array(
                'description' => 'наличие холодильника (строго ограниченные значения — «да»/«нет», «true»/«false», «1»/«0», «+»/«-»)',
                'requried'    => false,
                'fixed'       => true,
                'yesno'       => array_merge_recursive(App_Resources_General::getYes(), App_Resources_General::getNo()),
                'type'        => 'bool'
            ),
            'balcony' => array(
                'description' => 'тип балкона (рекомендуемые значения — «балкон», «лоджия», «2 балкона», «2 лоджии»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getBalcony(),
                'type'        => 'varchar'
            ),
            'bathroom-unit' => array(
                'description' => 'тип санузла (рекомендуемые значения — «совмещенный», «раздельный», «2»)',
                'requried'    => false,
                'fixed'       => false,
                'values'      => $this->getBathroom(),
                'type'        => 'varchar'
            ),
            'floor-covering' => array(
                'description' => 'покрытие пола (рекомендуемые значения — «паркет», «ламинат», «ковролин», «линолеум»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getFloorCover(),
                'type'        => 'varchar'
            ),
            'window-view' => array(
                'description' => 'вид из окон (рекомендуемые значения — «во двор», «на улицу»)',
                'requried'    => false,
                'fixed'       => false,
                'values'       => $this->getWindowViews(),
                'type'        => 'varchar'
            ),
            'floor' => array(
                'description' => 'этаж',
                'requried'    => false,
                'type'        => 'int'
            )
        );

        $this->setConfig($config);
    }

    /**
     * @return array
     */
    static public function getWindowViews()
    {
        return array(
            "во двор",
            "на улицу"
        );
    }

    /**
     * @return array
     */
    static public function getRoomViews()
    {
        return array(
            "смежные",
            "раздельные"
        );
    }

    /**
     * @return array
     */
    static public function getBalcony()
    {
        return array(
            "балкон",
            "лоджия",
            "2 балкона",
            "2 лоджии"
        );
    }

    /**
     * @return array
     */
    static public function getBathroom()
    {
        return array(
            "совмещенный",
            "раздельный",
            "2"
        );
    }

    /**
     * @return array
     */
    static public function getFloorCover()
    {
        return array(
            "паркет",
            "ламинат",
            "ковролин",
            "линолеум"
        );
    }

    /**
     * @return array
     */
    public function getDependency()
    {
        return array(
            'квартира',
            'flat',
            'room',
        );
    }
}