<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Request.php
 */

class App_Request
{

    CONST FILE_NAME = 'test1.xml';

    CONST MYSQL_DB = 'mysql/jcat_2014-06-12.sql';

    /**
     * @var null
     */
    protected $_file = NULL;

    /**
     * @var bool
     */
    static protected $_validated = true;

    /**
     * @todo при вызове метода добавлять в стек сообщений сообщение об ошибке в случае если передается false
     * @param $val
     */
    static public function setValidated($val)
    {
        self::$_validated = self::$_validated && $val;
    }

    public function __construct()
    {
        if ($this->checkDB() && !$this->_file && file_exists(JCAT_ROOT . DIRECTORY_SEPARATOR . self::FILE_NAME))
        {
            $this->_file = JCAT_ROOT . DIRECTORY_SEPARATOR . self::FILE_NAME;
        }
    }

    /**
     *  парсилка
     */
    public function match()
    {
        if ($this->_file)
        {
            $xml = new SimpleXMLElement(file_get_contents($this->_file));

            $db = Core::DB();

            $resources = array('General', 'Seller', 'Object', 'Location', 'Flat', 'Country', 'Building', 'Price');
            //$resources = array('General', 'Flat');

            if (!empty($xml->offer))
            {
                $data = array();

                foreach($xml->offer as $offer)
                {
                    self::$_validated = true;

                    $offerUniqueId = $offer->attributes()->__toString();
                    $select = $db->select()->from('entity')->where('unique_id = ?', $offerUniqueId);
                    $result = $db->fetchAll($select);

                    if (!$result)
                    {
                        $data[$offerUniqueId] = array();

                        foreach($resources as $item)
                        {
                            $className = "App_Resources_{$item}";
                            $obj = new $className();
                            $obj->analize($offer);

                            $data[$offerUniqueId] = array_merge_recursive($data[$offerUniqueId], $obj->analize($offer));
                        }
                    }

                    if (!self::$_validated)
                    {
                        unset($data[$offerUniqueId]);
                    }
                }

                $flat = new App_Resources_Flat();
                $country = new App_Resources_Country();

                foreach($data as $id => $item)
                {
                    $category = $item['varchar']['category'];

                    if (in_array($category, $flat->getDependency()))
                    {
                        $fields = $country->getConfig();

                        foreach($fields as $key => $item)
                        {
                            unset($data[$id][$item['type']][$key]);
                        }
                    }
                    else
                    {
                        $fields = $flat->getConfig();

                        foreach($fields as $key => $item)
                        {
                            unset($data[$id][$item['type']][$key]);
                        }
                    }
                }
            }

            //die("!");
            $model = new App_Model($data);
            $model->save();
        }
    }

    /**
     * @return bool
     */
    public function checkDB()
    {
        $db = Core::DB();

        $db->query("SET NAMES 'utf8'");

        if (!count($db->listTables()))
        {
            $db->getConnection()->exec(file_get_contents(JCAT_ROOT . DIRECTORY_SEPARATOR . self::MYSQL_DB));
        }

        if (count($db->listTables()))
        {
            return true;
        }

        return false;
    }
}