<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique

 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Model.php
 */

class App_Model
{
    /**
     * @var array
     */
    protected $_data = array();

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        if ($data)
        {
            $this->_data = $data;
        }
    }

    public function save()
    {
        $this->saveAttributes()
             ->saveData();
    }

    /**
     * @return $this
     */
    protected function saveData()
    {
        $select = Core::DB()->select()->from('attributes');
        $attributes = Core::DB()->fetchAll($select);

        $this->prepareAttributes($attributes);

        Core::DB()->beginTransaction();

        //echo '<pre><br/>'; var_dump($this->_data); die();
        foreach($this->_data as $offerID => $offer)
        {
            $IDRecord = null;

            foreach($offer as $typeCode => $fields)
            {
                if ($typeCode=='entity')
                {
                    $arr =  array(
                        'url'         => $fields['url'],
                        'type'        => $fields['type'],
                        'unique_id'   => $offerID,
                        'creation-date'    => $fields['creation-date']
                    );

                    if (isset($fields['last-update-date']))
                        $arr['last-update-date'] = $fields['last-update-date'];

                    if (isset($fields['expire-date']))
                        $arr['expire-date'] = $fields['expire-date'];

                    if (Core::DB()->insert('entity', $arr)) $IDRecord = Core::DB()->lastInsertId();
                }
            }

            if (!$IDRecord)
            {
                Core::DB()->closeConnection();
            }
            else
            {
                $success = true;

                /* save attributes */
                foreach($offer as $typeCode => $fields)
                {
                    switch($typeCode)
                    {
                        case 'int':
                            foreach($fields as $field => $value)
                            {
                                if (isset($attributes[$field]))
                                {
                                    if (!Core::DB()->insert('entity_type_int', array(
                                        'value'   => is_array($value) ? $value[0] : $value,
                                        'unit'    => is_array($value) ? $value[1] : '',
                                        'class'   => '',
                                        'entity_id'    => $IDRecord,
                                        'attribute_id' => $attributes[$field]['attribute_id']
                                    ))) $success = $success && false;
                                }
                            }
                            break;
                        case 'varchar':
                            foreach($fields as $field => $value)
                            {
                                if (isset($attributes[$field]))
                                {
                                    if (!Core::DB()->insert('entity_type_varchar', array(
                                        'value'   => $value,
                                        'entity_id'    => $IDRecord,
                                        'attribute_id' => $attributes[$field]['attribute_id']
                                    ))) $success = $success && false;
                                }
                            }
                            break;
                        case 'text':
                            foreach($fields as $field => $value)
                            {
                                if (isset($attributes[$field]))
                                {
                                    if (!Core::DB()->insert('entity_type_text', array(
                                        'value'        => $value,
                                        'entity_id'    => $IDRecord,
                                        'attribute_id' => $attributes[$field]['attribute_id']
                                    ))) $success = $success && false;
                                }
                            }
                            break;
                        case 'image':
                            foreach($fields as $image)
                            {
                                if (!Core::DB()->insert('entity_type_image', array(
                                    'value'        => $image,
                                    'entity_id'    => $IDRecord,
                                    'attribute_id' => $attributes[$field]['attribute_id']
                                ))) $success = $success && false;
                            }
                            break;
                        case 'price':
                            foreach($fields as $price)
                            {
                                if (!Core::DB()->insert('entity_type_price', array(
                                    'value'        => $price['value'],
                                    'unit'         => isset($price['unit']) ? $price['unit'] : '',
                                    'currency'     => isset($price['currency']) ? $price['currency'] : '',
                                    'period'       => isset($price['period']) ? $price['period'] : '',
                                    'entity_id'    => $IDRecord
                                    //'attribute_id' => $attributes[$field]['attribute_id']
                                ))) $success = $success && false;
                            }
                            break;
                    }
                }
            }
        }

        //Core::DB()->closeConnection();

        if (!empty($success))
        {
            if ($success)
                Core::DB()->commit();
            else
                Core::DB()->closeConnection();
        }

        return $this;
    }

    /**
     * @return $this
     */
    protected function saveAttributes()
    {
        foreach($this->_data as $offerID => $offer)
        {
            $select = Core::DB()->select()->from('attributes', 'name');
            $attributes = Core::DB()->fetchCol($select);

            foreach($offer as $typeCode => $fields)
            {
                switch($typeCode)
                {
                    case 'image':
                        break;
                    case 'price':
                        break;
                    default:
                        foreach($fields as $fieldKey => $field)
                        {
                            if (!in_array($fieldKey, $attributes))
                            {
                                $result = Core::DB()->insert('attributes', array(
                                    'name'        => $fieldKey,
                                    'description' => '',
                                    'type'        => $typeCode
                                ));
                            }
                        }
                        break;
                }
            }
        }

        return $this;
    }

    /**
     * @param $attributes
     * @return $this
     */
    protected function prepareAttributes(&$attributes)
    {
        $data = array();

        foreach($attributes as $attr)
        {
            $data[$attr['name']] = $attr;
        }

        $attributes = $data;

        return $this;
    }
}