# ************************************************************
# Sequel Pro SQL dump
# Версия 4004
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: 127.0.0.1 (MySQL 5.5.36)
# Схема: jcat
# Время создания: 2014-06-12 01:53:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `attribute_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `description` text,
  `type` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;

INSERT INTO `attributes` (`attribute_id`, `name`, `description`, `type`)
VALUES
	(1,'type','','entity'),
	(2,'url','','entity'),
	(3,'creation-date','','entity'),
	(4,'last-update-date','','entity'),
	(5,'expire-date','','entity'),
	(6,'property-type','','varchar'),
	(7,'category','','varchar'),
	(8,'agency_name','','varchar'),
	(9,'agency_phone','','varchar'),
	(10,'agency_category','','varchar'),
	(11,'agency_url','','varchar'),
	(12,'agency_email','','varchar'),
	(13,'renovation','','varchar'),
	(14,'location_country','','varchar'),
	(15,'location_region','','varchar'),
	(16,'location_district','','varchar'),
	(17,'location_locality-name','','varchar'),
	(18,'location_direction','','varchar'),
	(19,'payed-adv','','int'),
	(20,'manually-added','','int'),
	(21,'area','','int'),
	(22,'lot-area','','int'),
	(23,'location_distance','','int'),
	(24,'phone','','int'),
	(25,'internet','','int'),
	(26,'room-furniture','','int'),
	(27,'electricity-supply','','int'),
	(28,'sewerage-supply','','int'),
	(29,'water-supply','','int'),
	(30,'heating-supply','','int'),
	(31,'sauna','','int'),
	(32,'gas-supply','','int'),
	(33,'floors-total','','int'),
	(34,'alarm','','int'),
	(35,'description','','text'),
	(36,'location_address','','varchar'),
	(37,'prepayment','','int'),
	(38,'rent-pledge','','int'),
	(39,'agent-fee','','int'),
	(40,'television','','int'),
	(41,'washing-machine','','int'),
	(42,'refrigerator','','int'),
	(43,'new-flat','','int'),
	(44,'rooms','','int'),
	(45,'location_sub-locality-name','','varchar'),
	(46,'location_metro','','varchar'),
	(47,'building-type','','varchar'),
	(48,'haggle','','int'),
	(49,'living-space','','int'),
	(50,'kitchen-space','','int'),
	(51,'kitchen-furniture','','int'),
	(52,'ceiling-height','','int'),
	(53,'lift','','int'),
	(54,'rubbish-chute','','int'),
	(55,'location_name','','varchar'),
	(56,'location_time-on-foot','','int'),
	(57,'1','','entity'),
	(58,'2','','entity'),
	(59,'1','','varchar'),
	(60,'1','','int'),
	(61,'3','','entity'),
	(62,'3','','varchar'),
	(63,'4','','varchar'),
	(64,'5','','varchar'),
	(65,'6','','varchar'),
	(66,'7','','varchar'),
	(67,'3','','int'),
	(68,'4','','int'),
	(69,'5','','int'),
	(70,'6','','int'),
	(71,'7','','int'),
	(72,'8','','int'),
	(73,'9','','int'),
	(74,'10','','int'),
	(75,'11','','int'),
	(76,'12','','int'),
	(77,'13','','int'),
	(78,'14','','int'),
	(79,'15','','int'),
	(80,'16','','int'),
	(81,'17','','int'),
	(82,'18','','int'),
	(83,'agency_organization','','varchar'),
	(84,'location_railway-station','','varchar'),
	(85,'shower','','varchar'),
	(86,'toilet','','varchar'),
	(87,'mortgage','','int'),
	(88,'pmg','','int'),
	(89,'pool','','int'),
	(90,'kitchen','','int'),
	(91,'19','','int'),
	(92,'agency_agency-id','','varchar'),
	(93,'is-elite','','int'),
	(94,'billiard','','int'),
	(95,'20','','int'),
	(96,'21','','int'),
	(97,'parking','','int'),
	(98,'22','','int'),
	(99,'23','','int'),
	(100,'open-plan','','int'),
	(101,'building-name','','varchar'),
	(102,'built-year','','varchar'),
	(103,'with-pets','','int'),
	(104,'with-children','','int'),
	(105,'location_time-on-transport','','int'),
	(106,'lot-type','','int'),
	(107,'24','','int'),
	(108,'25','','int'),
	(109,'26','','int'),
	(110,'27','','int'),
	(111,'28','','int'),
	(112,'29','','int'),
	(113,'30','','int'),
	(114,'31','','int'),
	(115,'balcony','','varchar'),
	(116,'bathroom-unit','','varchar'),
	(117,'floor-covering','','varchar'),
	(118,'window-view','','varchar'),
	(119,'floor','','int');

/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы entity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity`;

CREATE TABLE `entity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` text,
  `creation-date` timestamp NULL DEFAULT NULL,
  `last-update-date` timestamp NULL DEFAULT NULL,
  `expire-date` timestamp NULL DEFAULT NULL,
  `type` varchar(128) DEFAULT NULL,
  `unique_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы entity_type_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity_type_image`;

CREATE TABLE `entity_type_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(512) DEFAULT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  `attribute_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `entity_type_image_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entity_type_image_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы entity_type_int
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity_type_int`;

CREATE TABLE `entity_type_int` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  `unit` varchar(256) DEFAULT NULL,
  `class` varchar(32) DEFAULT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  `attribute_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `entity_type_int_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `entity_type_int_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы entity_type_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity_type_price`;

CREATE TABLE `entity_type_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  `unit` varchar(32) DEFAULT NULL,
  `currency` varchar(32) DEFAULT NULL,
  `period` varchar(32) DEFAULT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  CONSTRAINT `entity_type_price_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы entity_type_text
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity_type_text`;

CREATE TABLE `entity_type_text` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` text,
  `entity_id` int(11) unsigned NOT NULL,
  `attribute_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `entity_type_text_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `entity_type_text_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Дамп таблицы entity_type_varchar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `entity_type_varchar`;

CREATE TABLE `entity_type_varchar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(256) DEFAULT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  `attribute_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`),
  CONSTRAINT `entity_type_varchar_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`attribute_id`) ON DELETE CASCADE,
  CONSTRAINT `entity_type_varchar_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
